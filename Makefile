up:
	docker-compose up -d

down:
	docker-compose down

restart: down up

build:
	docker-compose build

composer-update:
	docker-compose run --rm php-fpm composer update

composer-install:
	docker-compose run --rm php-fpm composer install

migrate-up:
	docker-compose run --rm php-fpm php yii migrate/up --interactive=0

migrate-down:
	docker-compose run --rm php-fpm php yii migrate/down --interactive=0

summary:
	docker-compose run --rm php-fpm php yii client/summary

profit:
	docker-compose run --rm php-fpm php yii client/profit

referral-current:
	docker-compose run --rm php-fpm php yii client/referral/current

referral-all:
	docker-compose run --rm php-fpm php yii client/referral/all

depth:
	docker-compose run --rm php-fpm php yii client/depth

