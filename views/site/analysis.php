<?php
/* @var $this \yii\web\View */
?>

<div class="site-analysis">
    <h1>Узкие места</h1>
    <ul>
        <li>Нет индексов.</li>
        <li>Нет внешних ключей, невозможно поставить огрничение внешнего ключа.</li>
        <li>Построение дерева клиентов возможно только через рекрсию.</li>
        <li>В таблице Trades дублируются поля id, trades.</li>
        <li>В таблице Trades, слишком много записей.</li>
    </ul>
</div>
