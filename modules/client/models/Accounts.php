<?php

namespace app\modules\client\models;

use yii\db\ActiveRecord;

/**
 * This is the model class for table "accounts".
 *
 * @property int $id
 * @property int|null $client_uid
 * @property int|null $login
 */
class Accounts extends ActiveRecord
{
    public static function tableName(): string
    {
        return 'accounts';
    }

    public function rules(): array
    {
        return [
            [['client_uid', 'login'], 'integer'],
        ];
    }

    public function attributeLabels(): array
    {
        return [
            'id' => 'ID',
            'client_uid' => 'Client Uid',
            'login' => 'Login',
        ];
    }
}
