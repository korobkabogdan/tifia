<?php

namespace app\modules\client\models;


use Yii;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "users".
 *
 * @property int $id
 * @property int|null $client_uid
 * @property string|null $email
 * @property string|null $gender
 * @property string|null $fullname
 * @property string|null $country
 * @property string|null $region
 * @property string|null $city
 * @property string|null $address
 * @property int|null $partner_id
 * @property string|null $reg_date
 * @property int|null $status
 * @property Trades[] $tradesRelation
 */
class Users extends ActiveRecord
{
    public static function tableName(): string
    {
        return 'users';
    }

    public function rules(): array
    {
        return [
            [['client_uid', 'partner_id', 'status'], 'integer'],
            [['reg_date'], 'safe'],
            [['email'], 'string', 'max' => 100],
            [['gender'], 'string', 'max' => 5],
            [['fullname'], 'string', 'max' => 150],
            [['country'], 'string', 'max' => 2],
            [['region', 'city'], 'string', 'max' => 50],
            [['address'], 'string', 'max' => 200],
        ];
    }

    public function attributeLabels(): array
    {
        return [
            'id' => 'ID',
            'client_uid' => 'Client Uid',
            'email' => 'Email',
            'gender' => 'Gender',
            'fullname' => 'Fullname',
            'country' => 'Country',
            'region' => 'Region',
            'city' => 'City',
            'address' => 'Address',
            'partner_id' => 'Partner ID',
            'reg_date' => 'Reg Date',
            'status' => 'Status',
        ];
    }

    private static function getSQLRecursive(): string
    {
        return "WITH RECURSIVE tree_referrals (id, client_uid, partner_id, path) AS
                   (
                       SELECT id, client_uid, partner_id, CAST(client_uid AS CHAR(64)) as path
                       FROM users
                       WHERE partner_id = :clientUid
                       UNION ALL
                       SELECT u.id, u.client_uid, u.partner_id, CONCAT(tr.path, '.', u.client_uid) as path
                       FROM tree_referrals AS tr JOIN users AS u ON tr.client_uid = u.partner_id
                   )";
    }

    public static function getTreeReferrals(int $clientUid = 0): array
    {
        $command = Yii::$app->db
            ->createCommand(self::getSQLRecursive() . "SELECT * FROM tree_referrals ORDER BY path;")
            ->bindValue('clientUid', $clientUid);

        return $command->queryAll();
    }

    public static function getTreeReferralsCount(int $clientUid = 0): int
    {
        $command = Yii::$app->db
            ->createCommand(self::getSQLRecursive() . "SELECT COUNT(id) FROM tree_referrals;")
            ->bindValue('clientUid', $clientUid);

        return $command->queryScalar();
    }

    public static function getTreeReferralsDepth(int $clientUid = 0): int
    {
        $command = Yii::$app->db
            ->createCommand("
                WITH RECURSIVE tree_referrals (id, client_uid, partner_id, lvl) AS
                   (
                       SELECT id, client_uid, partner_id, 1 lvl
                       FROM users
                       WHERE client_uid = :clientUid
                       UNION ALL
                       SELECT t.id, t.client_uid, t.partner_id, tr.lvl + 1
                       FROM tree_referrals AS tr JOIN users AS t
                                                 ON tr.client_uid = t.partner_id
                   )
                SELECT MAX(lvl) FROM tree_referrals;
            ")
            ->bindValue('clientUid', $clientUid);

        return $command->queryScalar();
    }

    public function getTradesRelation(): ActiveQuery
    {
        return $this->hasMany(Trades::class, ['login' => 'login'])
            ->viaTable(Accounts::tableName(), ['client_uid' => 'client_uid']);
    }
}
