<?php


namespace app\modules\client\commands;


use app\modules\client\models\Users;
use yii\console\Controller;
use yii\console\ExitCode;
use yii\helpers\Console;

class ReferralController extends Controller
{
    public function actionCurrent(int $partnerId = 82824897): int
    {
        $start = microtime(true);
        $this->stdout('START' . PHP_EOL, Console::FG_GREEN);

        $count = Users::find()
            ->where(['partner_id' => $partnerId])
            ->count();

        $this->stdout($count . PHP_EOL, Console::FG_CYAN);
        $this->stdout('END. Script execution time ' . $time = microtime(true) - $start . PHP_EOL, Console::FG_GREEN);

        return ExitCode::OK;
    }

    public function actionAll(int $partnerId = 82824897): int
    {
        $start = microtime(true);
        $this->stdout('START' . PHP_EOL, Console::FG_GREEN);

        $count = Users::getTreeReferralsCount($partnerId);

        $this->stdout($count . PHP_EOL, Console::FG_CYAN);
        $this->stdout('END. Script execution time ' . $time = microtime(true) - $start . PHP_EOL, Console::FG_GREEN);

        return ExitCode::OK;
    }
}