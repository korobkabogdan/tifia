<?php


namespace app\modules\client\commands;


use app\modules\client\models\Trades;
use yii\console\Controller;
use yii\console\ExitCode;
use yii\helpers\Console;

class ProfitController extends Controller
{
    public function actionIndex(string $dateStart = '2000-01-01', string $dateEnd = '2020-10-04'): int
    {
        $start = microtime(true);
        $this->stdout('START' . PHP_EOL, Console::FG_GREEN);

        $sum = Trades::find()
            ->where(['BETWEEN', 'close_time', $dateStart, $dateEnd])
            ->sum('profit');

        $this->stdout((float)$sum . PHP_EOL, Console::FG_CYAN);
        $this->stdout('END. Script execution time ' . $time = microtime(true) - $start . PHP_EOL, Console::FG_GREEN);

        return ExitCode::OK;
    }
}