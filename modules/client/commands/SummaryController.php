<?php


namespace app\modules\client\commands;


use app\modules\client\models\Users;
use yii\console\Controller;
use yii\console\ExitCode;
use yii\helpers\Console;

class SummaryController extends Controller
{
    private const PART = 1000;

    public function actionIndex(int $clientUid = 82824897, string $dateStart = '2000-01-01', string $dateEnd = '2020-10-05'): int
    {
        $start = microtime(true);
        $this->stdout('START' . PHP_EOL, Console::FG_GREEN);

        $referrals = Users::getTreeReferrals($clientUid);
        $ids = [];
        foreach ($referrals as $referral) {
            $ids[] = $referral['id'];
        }

        $query = Users::find()->where(['id' => $ids]);
        $total = $query->count();
        $users = $query->each(self::PART);

        $result = 0;
        /** @var Users $user */
        foreach ($users as $key => $user) {
            $trades = $user
                ->getTradesRelation()
                ->andWhere(['BETWEEN', 'close_time', $dateStart, $dateEnd])
                ->each(self::PART);

            foreach ($trades as $trade) {
                $result += $trade->volume * $trade->coeff_h * $trade->coeff_cr;
            }

            $this->showStatus($key + 1, $total);
        }

        $this->stdout(PHP_EOL . (float)$result . PHP_EOL, Console::FG_CYAN);
        $this->stdout('END. Script execution time ' . $time = microtime(true) - $start . PHP_EOL, Console::FG_GREEN);

        return ExitCode::OK;
    }

    protected function showStatus($done, $total, $size = 30): void
    {
        static $start_time;

        if ($done > $total) return;

        if (empty($start_time)) $start_time = time();
        $now = time();

        $perc = (double)($done / $total);

        $bar = floor($perc * $size);

        $status_bar = "\r[";
        $status_bar .= str_repeat("=", $bar);
        if ($bar < $size) {
            $status_bar .= ">";
            $status_bar .= str_repeat(" ", $size - $bar);
        } else {
            $status_bar .= "=";
        }

        $disp = number_format($perc * 100, 0);

        $status_bar .= "] $disp%  $done/$total";

        $rate = ($now - $start_time) / $done;
        $left = $total - $done;
        $eta = round($rate * $left, 2);

        $elapsed = $now - $start_time;

        $status_bar .= " remaining: " . number_format($eta) . " sec.  elapsed: " . number_format($elapsed) . " sec.";

        echo "$status_bar  ";

        flush();

        if ($done == $total) {
            echo "\n";
        }
    }
}