<?php


namespace app\modules\client\commands;


use app\modules\client\models\Users;
use yii\console\Controller;
use yii\console\ExitCode;
use yii\helpers\Console;

class DepthController extends Controller
{
    public function actionIndex(int $partnerId = 0): int
    {
        $start = microtime(true);
        $this->stdout('START' . PHP_EOL, Console::FG_GREEN);

        $maxDepth = Users::getTreeReferralsDepth($partnerId);

        $this->stdout((int)$maxDepth . PHP_EOL, Console::FG_CYAN);
        $this->stdout('END. Script execution time ' . $time = microtime(true) - $start . PHP_EOL, Console::FG_GREEN);

        return ExitCode::OK;
    }
}