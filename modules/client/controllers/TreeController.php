<?php


namespace app\modules\client\controllers;


use app\modules\client\models\Users;
use yii\web\Controller;

class TreeController extends Controller
{
    public function actionIndex(): string
    {
        $referrals = Users::getTreeReferrals();
        return $this->render('index', ['referrals' => $referrals]);
    }
}