<h1>Тестовое задание для Tifia.com</h1>
https://bitbucket.org/alexgutnik/test-task/src/master/

Утановка и настройка проекта
-------------------
Установить Docker
```
https://www.docker.com/get-started
```

Создать .env файл
```
cp .env.example .env
```

Поднять контейнеры
```
docker-compose up -d
```

Установить библиотеки из composer
```
docker-compose run --rm php-fpm composer install
```

Запуск консольных скриптов
-------------------
Посчитать суммарный объем volume * coeff_h * coeff_cr по всем уровням реферральной системы за период времени
```
docker-compose run --rm php-fpm php yii client/summary 82824897 2000-01-01 2020-10-05
```

Посчитать прибыльность (сумма profit) за определенный период времени
```
docker-compose run --rm php-fpm php yii client/profit 2000-01-01 2020-10-05
```

Посчитать количество прямых рефералов и количество всех рефералов клиента
```
docker-compose run --rm php-fpm php yii client/referral/current 82824897
docker-compose run --rm php-fpm php yii client/referral/all 82824897
```

Посчитать количество уровней реферальной сетки
```
docker-compose run --rm php-fpm php yii client/depth 0
```