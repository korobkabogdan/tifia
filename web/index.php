<?php
require __DIR__ . '/../vendor/autoload.php';

(new Symfony\Component\Dotenv\Dotenv())->load(__DIR__ . '/../.env');

define('YII_DEBUG', $_ENV['APP_DEBUG']);
define('YII_ENV', $_ENV['APP_ENV']);

require __DIR__ . '/../vendor/yiisoft/yii2/Yii.php';

$config = require __DIR__ . '/../config/web.php';

(new yii\web\Application($config))->run();
